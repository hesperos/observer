#include <memory>
#include <vector>
#include <iostream>

class Observer {
public:
    virtual ~Observer() = default;

    virtual void updateA(int a) = 0;

    virtual void updateB(std::string b) = 0;
};

template <typename ObserverT>
class Observed {
public:
    void addObserver(std::shared_ptr<ObserverT> observer) {
        observers.push_back(observer);
    }

    void removeObserver(std::shared_ptr<ObserverT> observer) {
        observers.erase(std::remove_if(observers.begin(), observers.end(),
            [observer](const auto o){ return o.lock() == observer; }),
            observers.end());
    }

    template <typename ReturnT, typename ... ArgsT>
    void notify(ReturnT (ObserverT::*member)(ArgsT...), ArgsT&& ... args) {
        for (const auto o : observers) {
            if (auto strong = o.lock()) {
                (strong.get()->*member)(std::forward<ArgsT>(args)...);
            }
        }
    }

protected:
    std::vector<std::weak_ptr<ObserverT>> observers;
};

class ClientConcreteObserver : public Observer {
public:
    void updateA(int a) {
        std::cout << "got update A with value: " << a << std::endl;
    }

    void updateB(std::string b) {
        std::cout << "got update B with value: " << b << std::endl;
    }
};

class ClientObserved : public Observed<Observer> {
public:
};

int main() {
    auto co = std::make_shared<ClientConcreteObserver>();
    ClientObserved o;

    o.addObserver(co);
    o.notify(&Observer::updateA, 456);
    o.notify(&Observer::updateB, std::string("hello"));

    return 0;
}